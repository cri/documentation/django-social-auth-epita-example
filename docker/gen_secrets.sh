#!/bin/sh

RAND_SECRET_NAMES="django-secret-key postgres-passwd"
USER_SECRET_NAMES="django-oidc-access-key django-oidc-secret-key"

for name in ${RAND_SECRET_NAMES}; do
	DST=./secrets/$name
	if [ ! -e ${DST} ]; then
		tr -dc 'a-zA-Z0-9' < /dev/urandom | fold -w 64 | head -n 1 \
			> ${DST}
	fi
done

for name in ${USER_SECRET_NAMES}; do
	DST=./secrets/$name
	envname=$(echo $name | sed 's/-/_/g')
	envvalue=$(eval echo \${$envname})
	if [ ! -z $envvalue ]; then
		echo $envvalue > ${DST}
		continue
	fi
	if [ ! -e ${DST} ]; then
		echo -n "Please input a value for ${name}: "
		read value
		echo "${value}" > ${DST}
	fi
done
