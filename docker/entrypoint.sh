#!/bin/sh

set -e

mkdir -p $HOME/.ipython/profile_default/
cp docker/config/ipython_config.py $HOME/.ipython/profile_default/

if [ "$#" -eq 0 ]; then
	if [ -n "$DEV" ]; then
		set -- ./manage.py runserver 0.0.0.0:8000
	else
		set -- gunicorn django_social_auth_epita_example.wsgi \
			-b 0.0.0.0:8000
	fi

	if [ -n "$DEV" ] || [ -n "$INIT_JOB" ]; then
		./manage.py collectstatic --noinput
		./manage.py migrate
	else
		./manage.py check --deploy
	fi
fi

exec "$@"
