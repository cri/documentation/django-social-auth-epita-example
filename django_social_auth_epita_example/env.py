from os import environ


class MissingEnvVariable(RuntimeError):
    def __init__(self, name):
        super().__init__(f"`{name}` should be present in the environment.")


FALSE_VALUES = ("0", "off", "false", "no")
NO_DEFAULT = object()


def ignore_missing():
    if "DJANGO_IGNORE_MISSING_ENV" not in environ:
        return False
    return environ.get("DJANGO_IGNORE_MISSING_VALUES") not in FALSE_VALUES


def get_bool(name, default=NO_DEFAULT):
    if name not in environ:
        if default is not NO_DEFAULT:
            return default
        if ignore_missing():
            return None
        raise MissingEnvVariable(name)
    return environ.get(name).lower() not in FALSE_VALUES


def get_string(name, default=NO_DEFAULT):
    if name not in environ and default is NO_DEFAULT:
        if ignore_missing():
            return "dummy"
        raise MissingEnvVariable(name)
    return environ.get(name, default)


def get_list(name, default=NO_DEFAULT, separator=None):
    if name not in environ:
        if default is not NO_DEFAULT:
            return default
        if ignore_missing():
            return []
        raise MissingEnvVariable(name)
    return environ.get(name).split(separator)


def get_secret(name, default=NO_DEFAULT):
    alt_name = f"{name}_FILE"
    if name not in environ and alt_name not in environ and default is NO_DEFAULT:
        if ignore_missing():
            return "dummy"
        raise MissingEnvVariable(name)
    if name in environ:
        return environ.get(name)
    if alt_name in environ:
        with open(environ.get(alt_name), "r") as f:
            return f.read().strip()
    return default
